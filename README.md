# Automate Variant interpretation for clinical setting #

This pipeline can be executed in your Linux environment (you must install all dependencies). You must install pandas, os, re, glob, csv, datetime, sys, subprocess.

### How to run the variant interpretation pipeline in our own Linux environment? ##

Please execute the file `python3 fourth_diagnosys_selezioneautomatica_NEW.py -pan panel_name  -proj project_name ` for variant selection and the file `python3 fourth_b_diagnosys_variantinterpretation.py -pan panel_name -proj project_name` to calculate the verdict of the variants.

#### Input project folder ##
Give in input the folder in which the analysis must be executed and the name of the panel.

#### Outputs ##
The output folder will be two folders 'final' and 'control' inside the project folder

### How to run the validation pipeline in our own Linux environment? ##

Please execute the file `python3 validazione_preselezione.py`.

#### Input folder ##
Give in input the files of the pre-selection (before filtering on the interpretation) and of the selection (final results of the selection) that were the result of the fourth_diagnosys_selezioneautomatica_NEW.py script.

#### Output/results ##
In the output directory you will find a folder with the date in which you run the code and an Excel file with the results of the validation.
