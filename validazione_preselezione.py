#!/home/bioinfo/VIRTUAL3/bin/python3.5
#24/08/2020

# -*- coding: utf-8 -*-


import pandas as pd
import numpy as np
from os.path import isfile, join
import os,re,glob,csv,argparse,datetime,sys,subprocess
from datetime import date

pd.options.display.float_format = '{:,.5f}'.format

path = os.getcwd()

####################################################

def check_decisionmaf(SAMPLE): #check if decisionMAF <3% a part from the exception, FP
	errors = 0
	passed = True
	SAMPLE_t = SAMPLE[(SAMPLE['definitivemaf']>0.03) & (SAMPLE['definitivemaf2']>0.03)]
	SAMPLE_t = SAMPLE_t[SAMPLE_t['Description']!='SELECTED']

	if SAMPLE_t.shape[0] != 0:
		errors = SAMPLE_t.shape[0]

	if errors != 0: passed = False
	return errors, passed

def check_verdictB(SAMPLE_pre,regioniproblematiche, SAMPLE, exception): #check if variants B and LB are filtered a part from exception
	regioniproblematiche = pd.read_csv(regioniproblematiche,sep='\t',header=0)[['tipo','hgvs','categoria']]
	regioniproblematiche['HGVS'] = regioniproblematiche['hgvs']
	FUNCTIONALPATHOG = regioniproblematiche[(regioniproblematiche['tipo']=='FUNCTIONAL IMPACT') & (regioniproblematiche['categoria'].str.contains('patogenetica'))]

	exceptions=pd.read_csv(exception)

	result = SAMPLE_pre[['sample','HGVS', 'Description', 'acmg_verdict']]
	result.loc[:, 'Exception'] = False
	result.loc[:, 'Pathogenic'] = False
	errorsFN = 0
	errorsFP = 0
	passed = True

	for index, row in exceptions.iterrows():
		if str(row.HGVS).split(':')[1] in list(result['HGVS'].str.split(':').str.get(1)):
			result.loc[result['HGVS'].str.contains(str(row.HGVS), case=False),'Exception'] = True


	for index, row in FUNCTIONALPATHOG.iterrows():
		if str(row.HGVS).split(':')[1] in list(result['HGVS'].str.split(':').str.get(1)):
			result.loc[result['HGVS'].str.contains(str(row.HGVS), case=False),'Pathogenic'] = True

	BnFile = result[((result['acmg_verdict'] == 'Benign') | (result['acmg_verdict'] == 'Likely Benign')) & (result['Pathogenic'] != True)]
	BnExc = result[((result['acmg_verdict'] == 'Benign') | (result['acmg_verdict'] == 'Likely Benign')) & (result['Exception'] != True)]
	BpExc = result[((result['acmg_verdict'] == 'Benign') | (result['acmg_verdict'] == 'Likely Benign')) & (result['Exception'] == True)]
	BpFile = result[((result['acmg_verdict'] == 'Benign') | (result['acmg_verdict'] == 'Likely Benign')) & (result['Pathogenic'] == True)]

	for index, row in BnFile.iterrows():#fp
		if str(row.HGVS) in SAMPLE.HGVS:
			errorsFP = errorsFP + 1
	for index, row in BnExc.iterrows():#fp
		if str(row.HGVS) in SAMPLE.HGVS:
			errorsFP = errorsFP + 1

	for index, row in BpExc.iterrows():#fn
		if str(row.HGVS) not in SAMPLE.HGVS:
			errorsFN = errorsFN + 1
	for index, row in BpFile.iterrows():#fn
		if str(row.HGVS) not in SAMPLE.HGVS:
			errorsFN = errorsFN + 1
	if (errorsFN + errorsFP) != 0: passed = False

	return errorsFN, errorsFP, passed

def check_verdictP(SAMPLE_pre,regioniproblematiche, SAMPLE):#check if variants P, VUS and LP are present a part from exception
	regioniproblematiche = pd.read_csv(regioniproblematiche,sep='\t',header=0)[['tipo','hgvs','categoria']]
	regioniproblematiche['HGVS'] = regioniproblematiche['hgvs']
	PROBLEMATICA = regioniproblematiche[regioniproblematiche['tipo']== 'PROBLEMATICA']
	NONREFERTABILE = regioniproblematiche[regioniproblematiche['tipo']== 'NON REFERTABILE']


	result = SAMPLE_pre[['sample','HGVS', 'Description', 'acmg_verdict', 'unbalance', 'consequence', 'definitivemaf', 'definitivemaf2']]
	result.loc[:, 'Problematica'] = False
	result.loc[:, 'Nonrefertabile'] = False
	errorsFN = 0
	errorsFP = 0
	passed = True


	for index, row in PROBLEMATICA.iterrows():
		for hgvs in list(SAMPLE_pre['HGVS']):
			START = int(str(row.HGVS).split(':')[1].split('-')[0])
			END = int(str(row.HGVS).split(':')[1].split('-')[1])
			if ((int(str(hgvs).split(':')[1].split('-')[0]) >= START) & (int(str(hgvs).split(':')[1].split('-')[0]) <= END)):
				hgvs = hgvs[0].lower() + hgvs[1:]
				result.loc[result['HGVS'] == hgvs,'Problematica'] = True


	for index, row in NONREFERTABILE.iterrows():
		for hgvs in list(SAMPLE_pre['HGVS']):
			START = int(str(row.HGVS).split(':')[1].split('-')[0])
			END = int(str(row.HGVS).split(':')[1].split('-')[1])
			if ((int(str(hgvs).split(':')[1].split('-')[0]) >= START) & (int(str(hgvs).split(':')[1].split('-')[0]) <= END)):
				hgvs = hgvs[0].lower() + hgvs[1:]
				result.loc[result['HGVS'] == hgvs,'Nonrefertabile'] = True


	PinFile = result[((result['acmg_verdict'] == 'Pathogenic') | (result['acmg_verdict'] == 'Likely Pathogenic') | (result['acmg_verdict'] == 'Uncertain Significance')) & ((result['Problematica'] != True) & (result['Nonrefertabile'] != True))]

	intr = PinFile[(PinFile['unbalance']=='del=nan') & (PinFile['acmg_verdict']=='Pathogenic')]
	PinFile = PinFile[(PinFile['unbalance']!='del=nan')]
	PinFile = pd.concat([PinFile, intr])

	synonimous = PinFile[((PinFile['consequence']=='synonymous_variant') & (PinFile['acmg_verdict']=='Pathogenic'))]
	PinFile = PinFile[(PinFile['consequence']!='synonymous_variant')]
	PinFile = pd.concat([PinFile,synonimous])

	for index, row in PinFile.iterrows():#fn
		if str(row.HGVS) not in list(SAMPLE.HGVS):
			if (row.definitivemaf<=0.03) & (row.definitivemaf2<=0.03):
				errorsFN = errorsFN + 1
	for index, row in SAMPLE_pre.iterrows():#fp
		if ((row['acmg_verdict'] == 'Pathogenic') | (row['acmg_verdict'] == 'Likely Pathogenic') | (row['acmg_verdict'] == 'Uncertain Significance')) & (row.HGVS in SAMPLE.HGVS) & (row.HGVS not in PinFile.HGVS):
			errorsFP = errorsFP + 1
	if (errorsFN + errorsFP) != 0: passed = False
	return errorsFP, errorsFN, passed

def check_from_file(SAMPLE_pre,regioniproblematiche, exception): #check if variants in problematic regions or in the exceptions file are reported
	errorsFN = 0
	passed = True
	regioniproblematiche = pd.read_csv(regioniproblematiche,sep='\t',header=0)[['tipo','hgvs','categoria']]
	regioniproblematiche['HGVS'] = regioniproblematiche['hgvs']
	PROBLEMATICA = regioniproblematiche[regioniproblematiche['tipo']== 'PROBLEMATICA']
	NONREFERTABILE = regioniproblematiche[regioniproblematiche['tipo']== 'NON REFERTABILE']
	FUNCTIONALPATHOG = regioniproblematiche[(regioniproblematiche['tipo']=='FUNCTIONAL IMPACT') & (regioniproblematiche['categoria'].str.contains('patogenetica'))]

	exceptions=pd.read_csv(exception)

	for index, row in PROBLEMATICA.iterrows():
		for hgvs in list(SAMPLE_pre['HGVS']):
			START = int(str(row.HGVS).split(':')[1].split('-')[0])
			END = int(str(row.HGVS).split(':')[1].split('-')[1])
			if ((int(str(hgvs).split(':')[1].split('-')[0]) >= START) & (int(str(hgvs).split(':')[1].split('-')[0]) <= END)):
				hgvs = hgvs[0].lower() + hgvs[1:]
				if SAMPLE_pre.loc[SAMPLE_pre['HGVS'] == hgvs, 'Description'].any() != 'NOT SELECTED':#fn
					errorsFN = errorsFN + 1

	for index, row in NONREFERTABILE.iterrows():
		for hgvs in list(SAMPLE_pre['HGVS']):
			START = int(str(row.HGVS).split(':')[1].split('-')[0])
			END = int(str(row.HGVS).split(':')[1].split('-')[1])
			if ((int(str(hgvs).split(':')[1].split('-')[0]) >= START) & (int(str(hgvs).split(':')[1].split('-')[0]) <= END)):
				hgvs = hgvs[0].lower() + hgvs[1:]
				if SAMPLE_pre.loc[SAMPLE_pre['HGVS'] == hgvs, 'Description'].any() != 'NOT SELECTED':#fn
					errorsFN = errorsFN + 1

	for index, row in FUNCTIONALPATHOG.iterrows():
		if str(row.HGVS).split(':')[1] in list(SAMPLE_pre['HGVS'].str.split(':').str.get(1)):
			if SAMPLE_pre.loc[SAMPLE_pre['HGVS'].str.contains(row.HGVS, case=False), 'Description'].any() != 'SELECTED':#fn
				errorsFN = errorsFN + 1

	for index, row in exceptions.iterrows():
		if row.HGVS in list(SAMPLE_pre['HGVS']):
			if SAMPLE_pre.loc[SAMPLE_pre['HGVS'].str.contains(row.HGVS, case=False), 'Description'].any() != 'SELECTED':
				errorsFN = errorsFN + 1

	if errorsFN != 0: passed = False

	return errorsFN,passed

def check_report_from_file(SAMPLE_pre,regioniproblematiche): #check if variants in files from benignity and omologies are reported

	errors = 0
	passed = True
	regioniproblematiche = pd.read_csv(regioniproblematiche,sep='\t',header=0)[['tipo','hgvs','categoria']]
	regioniproblematiche['HGVS'] = regioniproblematiche['hgvs']
	OMOLOGIA = regioniproblematiche[regioniproblematiche['tipo']=='OMOLOGIA']
	FUNCTIONALBENIGN = regioniproblematiche[(regioniproblematiche['tipo']=='FUNCTIONAL IMPACT') & (regioniproblematiche['categoria'].str.contains('benigna'))]

	for index, row in FUNCTIONALBENIGN.iterrows():
		if str(row.HGVS).split(':')[1] in list(SAMPLE_pre['HGVS'].str.split(':').str.get(1)):
			if SAMPLE_pre.loc[SAMPLE_pre['HGVS'].str.contains(row.HGVS, case=False), 'comments'].any() != 'Benigna from FILE':
				if SAMPLE_pre.loc[SAMPLE_pre['HGVS'].str.contains(row.HGVS, case=False), 'Description'].any() != 'NOT SELECTED':
					errors = errors + 1

	for index, row in OMOLOGIA.iterrows():
		for hgvs in list(SAMPLE_pre['HGVS']):
			START = int(str(row.HGVS).split(':')[1].split('-')[0])
			END = int(str(row.HGVS).split(':')[1].split('-')[1])

			if ((int(str(hgvs).split(':')[1].split('-')[0]) >= START) & (int(str(hgvs).split(':')[1].split('-')[0]) <= END)):
				if (SAMPLE_pre.loc[SAMPLE_pre['HGVS'] == hgvs, 'comments'].any() != 'Omologia from FILE') & (SAMPLE_pre.loc[SAMPLE_pre['HGVS'] == hgvs, 'comments'].any() != 'Benigna from FILE'):
					if SAMPLE_pre.loc[SAMPLE_pre['HGVS'].str.contains(row.HGVS, case=False), 'Description'].any() != 'NOT SELECTED':
						errors = errors + 1
	if errors != 0: passed = False
	return errors, passed



if __name__=="__main__":

	exception='eccezioni.csv'
	regioniproblematiche= 'regioniproblematiche.txt'
	folderINPUT = join(path, 'INPUT')
	folderOUTPUT = join(path, 'OUTPUT')

	files=sorted(glob.glob(join(folderINPUT,'*_PREvariant_selection.csv')))
	today = date.today()
	d1 = today.strftime("%d_%m_%Y")
	try:
		os.mkdir(folderOUTPUT+'/'+ d1)
	except:
		pass
	# Create a Pandas Excel writer using XlsxWriter as the engine.
	writer = pd.ExcelWriter(''.join([folderOUTPUT,'/', d1,'/',d1,'_validation.xlsx']))
	validazione = {}
	errori={}
	for file in set(files):
		sample = file.split('/')[-1].split('_')[0]
		SAMPLE = pd.read_csv(''.join([folderINPUT,'/',sample, '_variant_selection.csv']), sep ='\t')
		SAMPLE_pre = pd.read_csv(''.join([folderINPUT,'/',sample, '_PREvariant_selection.csv']), sep ='\t')
		os.rename(''.join([folderINPUT,'/',sample, '_variant_selection.csv']), ''.join([folderOUTPUT,'/', d1, '/',sample,'_variant_selection.csv']))
		os.rename(''.join([folderINPUT,'/',sample, '_PREvariant_selection.csv']), ''.join([folderOUTPUT,'/', d1, '/',sample,'_PREvariant_selection.csv']))
		eFP1, pass1 = check_decisionmaf(SAMPLE)
		eFN2, eFP2, pass2 = check_verdictB(SAMPLE_pre, regioniproblematiche, SAMPLE, exception)
		eFP3, eFN3, pass3 = check_verdictP(SAMPLE_pre,regioniproblematiche, SAMPLE)
		eFN4, pass4 = check_from_file(SAMPLE_pre,regioniproblematiche, exception)
		eFN5, pass5 = check_report_from_file(SAMPLE_pre,regioniproblematiche)
		passFinale = pass1 & pass2 & pass3 & pass4 & pass5
		eFN = eFN2 + eFN3 + eFN4 + eFN5
		eFP = eFP1 + eFP2 + eFP3
		TP = SAMPLE.shape[0]-eFP
		TN = SAMPLE_pre.shape[0]-SAMPLE.shape[0]-eFN
		errori[sample] = {'FN': eFN, 'FP':eFP, '# errors': eFN+eFP, 'TN': TN, 'TP': TP, 'Sensitivity': ((TP/(TP+eFN))*100), 'Specificity': ((TN/(TN+eFP))*100), 'Accuracy': (((TP+TN)/(TP+TN+eFN+eFP))*100)}
		pd.DataFrame.from_dict(errori[sample], orient='index').T.to_excel(writer,sample)

	errori = pd.DataFrame.from_dict(errori, orient='index')
	errori = errori[['# errors', 'FP', 'FN', 'TP', 'TN', 'Sensitivity', 'Specificity', 'Accuracy']]
	errori = errori.append(pd.concat([errori.iloc[:, 0:5].sum(), errori.iloc[:, 5:].mean()]).rename('Total'))

	errori.to_excel(writer,'Results')
	writer.save()
