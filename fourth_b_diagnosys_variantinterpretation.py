#!/home/bioinfo/VIRTUAL3/bin/python3.5
#24/08/2020

# -*- coding: utf-8 -*-


# Load libraries
import os,re,glob,csv,argparse,datetime,sys,subprocess
from os.path import isfile, join
import first_diagnosys_core_V2 as step

import numpy as np
import pandas as pd
from varsome_api.client import VarSomeAPIClient,VarSomeAPIException
from varsome_api.models.variant import AnnotatedVariant
import sqlite3
from os import listdir, system
import requests
from requests.structures import CaseInsensitiveDict
from requests.auth import HTTPBasicAuth
from pandas.io.json import json_normalize
import json
import ast

path = os.getcwd()
geno37 = join('/home/bioinfo/','dataset/GENOME/37/all_chr37.fa')
geno38 = join('/home/bioinfo/','dataset/GENOME/38/all_chr38.fa')

#all_genes_exons37 = join('/home/bioinfo/','dataset/MAGIS/37/new15nt_all_genes_exons.txt')
#all_genes_exons38 = join('/home/bioinfo/','dataset/MAGIS/38/new15nt_all_genes_exons.txt')

HGMD_37 = join('/home/bioinfo/','dataset/HGMD/37/HGMD_pro_2014_2.bed')
HGMD_38 = join('/home/bioinfo/','dataset/HGMD/38/LiftOverHGMD_pro_2014_2.bed')

pd.options.display.float_format = '{:,.5f}'.format

strength_dict={'BA1':'Stand Alone','BS1':'Strong','BS2':'Strong','BS3':'Strong',
		'BS4':'Strong','BP1':'Supporting','BP2':'Supporting',
		'BP3':'Supporting','BP4':'Supporting','BP5':'Supporting',
		'BP6':'Supporting','BP7':'Supporting','PVS1':'Very Strong','PS1':'Strong',
		'PS2':'Strong','PS3':'Strong','PS4':'Strong','PM1':'Moderate','PM2':'Moderate','PM3':'Moderate',
		'PM4':'Moderate','PM5':'Moderate','PM6':'Moderate','PP1':'Supporting','PP2':'Supporting',
		'PP3':'Supporting','PP4':'Supporting','PP5':'Supporting'}

def InputPar():
	parser = argparse.ArgumentParser(prog= 'MAGI EUREGIO DIAGNOSYS',description='Pipe coverage analysis')

	parser.add_argument('-p','--path', metavar='PATH', default=path,
						help='[Default = Pipe run in same folder when launch command]')

	parser.add_argument('-proj','--project',metavar='PROJECT NAME',
							help='Insert Family name to create Folder')

	parser.add_argument('-g','--genome', metavar='choose Assembling', choices=['geno37','geno38'],
						default='geno38',
						help='Choices: geno37, geno38 -Run Analysis with genome GRCh38 or GRCh37')

	parser.add_argument('-d','--dest', metavar='destination', choices=['b','r','s','z'],
						required=True,
						help='Choices destination: b = bolzano; r = rovereto; s = sanfelice; z = ricerca,  - required ')

	parser.add_argument('-pan','--panel',metavar='PANEL',required=True,
				help='Pannelli Consigliati: 1:CLM2; 2:OSSNEW; 3:OCULARE; 4:OCULARE2; 5:SIFSR; 6:ALLGENE;'
					'7:trusightone; 8:GENETEST1; 9:GENETEST2; 10:CANCER; 11:MALFORMATIONI VASCOLARI;'
					'12:ANOMALIE VASCOLARI; 13:INFERTILITA; 14:OBESITA; 15:GENODERMATOSI\nRequired')

	parser.add_argument('-ovr','--over', metavar='choose Assembling', choices=['True','False'],
							default='False',
							help='Choices: True if you want create new Folder, False to write in just exist folder')

	return parser.parse_args()
#############################################################################
def fill_checklist_model(SAMPLE_model, SAMPLE_verdict):
	#print('fill_checklist_model')
	for i,row in SAMPLE_verdict.iterrows():
		if (row.acmg_classificatins) == 'Exception da Verificate!!!':
			explanation = ()
		else:
			row.acmg_classificatins = row.acmg_classificatins.replace('â\\x87\\x92', '>')
			row.acmg_classificatins = row.acmg_classificatins.replace('%%', '')
			explanation = ast.literal_eval(row.acmg_classificatins)
		for tuple in explanation:
			if len(tuple)==3:
				punto = tuple[0]
				strength = tuple[1]
				cause = tuple[2]
			elif len(tuple)==2:
				punto = tuple[0]
				strength = strength_dict[punto]
				cause = tuple[1]

			SAMPLE_model.loc[i, '_'.join([punto, 'final'])] = 1
			if (punto[0]=='B') & (strength == 'Moderate'): #all benign point if moderate, set to default
				print('Benign moderate')
				SAMPLE_model.loc[i, '_'.join([punto, 'intensita_final'])] = strength_dict[punto]
			else:
				SAMPLE_model.loc[i, '_'.join([punto, 'intensita_final'])] = strength
			if (punto[0]=='B') & (strength == 'Very Strong'): #all benign point if very strong set to default
				print('Benign Very Strong')
				SAMPLE_model.loc[i, '_'.join([punto, 'intensita_final'])] = strength_dict[punto]
			else:
				SAMPLE_model.loc[i, '_'.join([punto, 'intensita_final'])] = strength
			SAMPLE_model.loc[i, '_'.join([punto, 'cause_final'])] = cause

		if explanation!=():
			SAMPLE_model.loc[i,'choice_interpretation_final'] = variant_interpretation(SAMPLE_model.drop(SAMPLE_model.index.difference([i])))
		else:
			SAMPLE_model.loc[i,'choice_interpretation_final'] = 'UNKNOWN'
			SAMPLE_model.loc[i,'choice_interpretation'] = 'UNKNOWN'
		# print(SAMPLE_model.loc[i, 'choice_interpretation'])
		# print(SAMPLE_model.loc[i,'choice_interpretation_final'])

		# if SAMPLE_model.loc[i,'choice_interpretation_final']!=SAMPLE_model.loc[i, 'choice_interpretation']: #print quando i verdetti sono diversi
		# 	print(row[['HGVS']])
		# 	print(SAMPLE_model.loc[i, 'choice_interpretation'])
		# 	print(SAMPLE_model.loc[i,'choice_interpretation_final'])
		# 	print(explanation)
	SAMPLE_model['BA1_intensita_final'].fillna(strength_dict['BA1'],inplace=True)
	SAMPLE_model['BS1_intensita_final'].fillna(strength_dict['BS1'],inplace=True)
	SAMPLE_model['BS2_intensita_final'].fillna(strength_dict['BS2'],inplace=True)
	SAMPLE_model['BS3_intensita_final'].fillna(strength_dict['BS3'],inplace=True)
	SAMPLE_model['BS4_intensita_final'].fillna(strength_dict['BS4'],inplace=True)
	SAMPLE_model['BP1_intensita_final'].fillna(strength_dict['BP1'],inplace=True)
	SAMPLE_model['BP2_intensita_final'].fillna(strength_dict['BP2'],inplace=True)
	SAMPLE_model['BP3_intensita_final'].fillna(strength_dict['BP3'],inplace=True)
	SAMPLE_model['BP4_intensita_final'].fillna(strength_dict['BP4'],inplace=True)
	SAMPLE_model['BP5_intensita_final'].fillna(strength_dict['BP5'],inplace=True)
	SAMPLE_model['BP6_intensita_final'].fillna(strength_dict['BP6'],inplace=True)
	SAMPLE_model['BP7_intensita_final'].fillna(strength_dict['BP7'],inplace=True)
	SAMPLE_model['PVS1_intensita_final'].fillna(strength_dict['PVS1'],inplace=True)
	SAMPLE_model['PS1_intensita_final'].fillna(strength_dict['PS1'],inplace=True)
	SAMPLE_model['PS2_intensita_final'].fillna(strength_dict['PS2'],inplace=True)
	SAMPLE_model['PS3_intensita_final'].fillna(strength_dict['PS3'],inplace=True)
	SAMPLE_model['PS4_intensita_final'].fillna(strength_dict['PS4'],inplace=True)
	SAMPLE_model['PM1_intensita_final'].fillna(strength_dict['PM1'],inplace=True)
	SAMPLE_model['PM2_intensita_final'].fillna(strength_dict['PM2'],inplace=True)
	SAMPLE_model['PM3_intensita_final'].fillna(strength_dict['PM3'],inplace=True)
	SAMPLE_model['PM4_intensita_final'].fillna(strength_dict['PM4'],inplace=True)
	SAMPLE_model['PM5_intensita_final'].fillna(strength_dict['PM5'],inplace=True)
	SAMPLE_model['PM6_intensita_final'].fillna(strength_dict['PM6'],inplace=True)
	SAMPLE_model['PP1_intensita_final'].fillna(strength_dict['PP1'],inplace=True)
	SAMPLE_model['PP2_intensita_final'].fillna(strength_dict['PP2'],inplace=True)
	SAMPLE_model['PP3_intensita_final'].fillna(strength_dict['PP3'],inplace=True)
	SAMPLE_model['PP4_intensita_final'].fillna(strength_dict['PP4'],inplace=True)
	SAMPLE_model['PP5_intensita_final'].fillna(strength_dict['PP5'],inplace=True)

	SAMPLE_model['BA1_final'].fillna(0,inplace=True)
	SAMPLE_model['BS1_final'].fillna(0,inplace=True)
	SAMPLE_model['BS2_final'].fillna(0,inplace=True)
	SAMPLE_model['BS3_final'].fillna(0,inplace=True)
	SAMPLE_model['BS4_final'].fillna(0,inplace=True)
	SAMPLE_model['BP1_final'].fillna(0,inplace=True)
	SAMPLE_model['BP2_final'].fillna(0,inplace=True)
	SAMPLE_model['BP3_final'].fillna(0,inplace=True)
	SAMPLE_model['BP4_final'].fillna(0,inplace=True)
	SAMPLE_model['BP5_final'].fillna(0,inplace=True)
	SAMPLE_model['BP6_final'].fillna(0,inplace=True)
	SAMPLE_model['BP7_final'].fillna(0,inplace=True)
	SAMPLE_model['PVS1_final'].fillna(0,inplace=True)
	SAMPLE_model['PS2_final'].fillna(0,inplace=True)
	SAMPLE_model['PS3_final'].fillna(0,inplace=True)
	SAMPLE_model['PS1_final'].fillna(0,inplace=True)
	SAMPLE_model['PS4_final'].fillna(0,inplace=True)
	SAMPLE_model['PM1_final'].fillna(0,inplace=True)
	SAMPLE_model['PM2_final'].fillna(0,inplace=True)
	SAMPLE_model['PM3_final'].fillna(0,inplace=True)
	SAMPLE_model['PM4_final'].fillna(0,inplace=True)
	SAMPLE_model['PM5_final'].fillna(0,inplace=True)
	SAMPLE_model['PM6_final'].fillna(0,inplace=True)
	SAMPLE_model['PP1_final'].fillna(0,inplace=True)
	SAMPLE_model['PP2_final'].fillna(0,inplace=True)
	SAMPLE_model['PP3_final'].fillna(0,inplace=True)
	SAMPLE_model['PP4_final'].fillna(0,inplace=True)
	SAMPLE_model['PP5_final'].fillna(0,inplace=True)

	return SAMPLE_model

def variant_interpretation(variant):
	#print('variant_interpretation')
	variant['BA1_intensita_final'].fillna(strength_dict['BA1'],inplace=True)
	variant['BS1_intensita_final'].fillna(strength_dict['BS1'],inplace=True)
	variant['BS2_intensita_final'].fillna(strength_dict['BS2'],inplace=True)
	variant['BS3_intensita_final'].fillna(strength_dict['BS3'],inplace=True)
	variant['BS4_intensita_final'].fillna(strength_dict['BS4'],inplace=True)
	variant['BP1_intensita_final'].fillna(strength_dict['BP1'],inplace=True)
	variant['BP2_intensita_final'].fillna(strength_dict['BP2'],inplace=True)
	variant['BP3_intensita_final'].fillna(strength_dict['BP3'],inplace=True)
	variant['BP4_intensita_final'].fillna(strength_dict['BP4'],inplace=True)
	variant['BP5_intensita_final'].fillna(strength_dict['BP5'],inplace=True)
	variant['BP6_intensita_final'].fillna(strength_dict['BP6'],inplace=True)
	variant['BP7_intensita_final'].fillna(strength_dict['BP7'],inplace=True)
	variant['PVS1_intensita_final'].fillna(strength_dict['PVS1'],inplace=True)
	variant['PS1_intensita_final'].fillna(strength_dict['PS1'],inplace=True)
	variant['PS2_intensita_final'].fillna(strength_dict['PS2'],inplace=True)
	variant['PS3_intensita_final'].fillna(strength_dict['PS3'],inplace=True)
	variant['PS4_intensita_final'].fillna(strength_dict['PS4'],inplace=True)
	variant['PM1_intensita_final'].fillna(strength_dict['PM1'],inplace=True)
	variant['PM2_intensita_final'].fillna(strength_dict['PM2'],inplace=True)
	variant['PM3_intensita_final'].fillna(strength_dict['PM3'],inplace=True)
	variant['PM4_intensita_final'].fillna(strength_dict['PM4'],inplace=True)
	variant['PM5_intensita_final'].fillna(strength_dict['PM5'],inplace=True)
	variant['PM6_intensita_final'].fillna(strength_dict['PM6'],inplace=True)
	variant['PP1_intensita_final'].fillna(strength_dict['PP1'],inplace=True)
	variant['PP2_intensita_final'].fillna(strength_dict['PP2'],inplace=True)
	variant['PP3_intensita_final'].fillna(strength_dict['PP3'],inplace=True)
	variant['PP4_intensita_final'].fillna(strength_dict['PP4'],inplace=True)
	variant['PP5_intensita_final'].fillna(strength_dict['PP5'],inplace=True)

	variant['BA1_final'].fillna(0,inplace=True)
	variant['BS1_final'].fillna(0,inplace=True)
	variant['BS2_final'].fillna(0,inplace=True)
	variant['BS3_final'].fillna(0,inplace=True)
	variant['BS4_final'].fillna(0,inplace=True)
	variant['BP1_final'].fillna(0,inplace=True)
	variant['BP2_final'].fillna(0,inplace=True)
	variant['BP3_final'].fillna(0,inplace=True)
	variant['BP4_final'].fillna(0,inplace=True)
	variant['BP5_final'].fillna(0,inplace=True)
	variant['BP6_final'].fillna(0,inplace=True)
	variant['BP7_final'].fillna(0,inplace=True)
	variant['PVS1_final'].fillna(0,inplace=True)
	variant['PS2_final'].fillna(0,inplace=True)
	variant['PS3_final'].fillna(0,inplace=True)
	variant['PS1_final'].fillna(0,inplace=True)
	variant['PS4_final'].fillna(0,inplace=True)
	variant['PM1_final'].fillna(0,inplace=True)
	variant['PM2_final'].fillna(0,inplace=True)
	variant['PM3_final'].fillna(0,inplace=True)
	variant['PM4_final'].fillna(0,inplace=True)
	variant['PM5_final'].fillna(0,inplace=True)
	variant['PM6_final'].fillna(0,inplace=True)
	variant['PP1_final'].fillna(0,inplace=True)
	variant['PP2_final'].fillna(0,inplace=True)
	variant['PP3_final'].fillna(0,inplace=True)
	variant['PP4_final'].fillna(0,inplace=True)
	variant['PP5_final'].fillna(0,inplace=True)

	benign_subscore = 'Uncertain Significance'
	pathogenic_subscore = 'Uncertain Significance'
	verdict = 'Uncertain Significance'
	punti_pat = ['PVS1', 'PS1', 'PS2', 'PS3', 'PS4', 'PM1', 'PM2', 'PM3', 'PM4', 'PM5', 'PM6', 'PP1', 'PP2', 'PP3', 'PP4', 'PP5']
	punti_ben = ['BA1', 'BS1', 'BS2', 'BS3', 'BS4', 'BP1', 'BP2', 'BP3', 'BP4', 'BP5', 'BP6', 'BP7']

	pat_model = {}
	index = variant.index[0]
	#dataframe of pathogenicity, rows the criteria and cols the 3 characteristics
	for p in punti_pat:
		#print(p)
		pat_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': variant.loc[index, p+'_intensita_final'], 'Causa': variant.loc[index, p+'_cause_final']}
	pat_model = pd.DataFrame.from_dict(pat_model, orient='index')
	# print(pathogenic_subscore)
	#print(pat_model, 'patmodel')
	#print(len(pat_model.loc[(pat_model['Intensita'].astype(str) != 'nan') & (pat_model['Intensita'].astype(str) != 'Very Strong') & (pat_model['Intensita'].astype(str) != 'Strong') & (pat_model['Intensita'].astype(str) != 'Moderate') & (pat_model['Intensita'].astype(str) != 'Supporting')]))
	#print(variant.loc[index, ''])
	#if variant
	if len(pat_model.loc[(pat_model['Intensita'].astype(str) != 'Very Strong') & (pat_model['Intensita'].astype(str) != 'Strong') & (pat_model['Intensita'].astype(str) != 'Moderate') & (pat_model['Intensita'].astype(str) != 'Supporting')]) != 0:
		#print('in')
		benign_subscore = 'UNKNOWN'
		pathogenic_subscore = 'UNKNOWN'
		verdict = 'UNKNOWN'
	#dataframe of benignity, rows the criteria and cols the 3 characteristics
	#print(pathogenic_subscore, 'ben2')
	ben_model = {}
	for p in punti_ben:
		#gestisco i diversi modi di scrivere Stand Alone
		if (p == 'BA1') & ((variant.loc[index, p+'_intensita_final'] == 'Stand-Alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand Alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand-alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand alone')):
			ben_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': 'Stand Alone', 'Causa': variant.loc[index, p+'_cause_final']}
		else:
			ben_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': variant.loc[index, p+'_intensita_final'], 'Causa': variant.loc[index, p+'_cause_final']}
	ben_model = pd.DataFrame.from_dict(ben_model, orient='index')

	if len(ben_model.loc[((ben_model['Intensita'].astype(str) != 'Stand Alone')) & (ben_model['Intensita'].astype(str) != 'Strong') & (ben_model['Intensita'].astype(str) != 'Supporting')]) != 0:
		benign_subscore = 'UNKNOWN'
		pathogenic_subscore = 'UNKNOWN'
		verdict = 'UNKNOWN'
	present_pat = pat_model[pat_model['Presenza']==1]
	pat_intensities = present_pat['Intensita'].value_counts()
	if 'Very Strong' not in pat_intensities:
		pat_intensities['Very Strong'] = 0
	if 'Strong' not in pat_intensities:
		pat_intensities['Strong'] = 0
	if 'Moderate' not in pat_intensities:
		pat_intensities['Moderate'] = 0
	if 'Supporting' not in pat_intensities:
		pat_intensities['Supporting'] = 0

	present_ben = ben_model[ben_model['Presenza']==1]
	ben_intensities = present_ben['Intensita'].value_counts()
	if 'Stand Alone' not in ben_intensities:
		ben_intensities['Stand Alone'] = 0
	if 'Strong' not in ben_intensities:
		ben_intensities['Strong'] = 0
	if 'Supporting' not in ben_intensities:
		ben_intensities['Supporting'] = 0

	###############Algorithm Varsome Interpratation#############################
	if pathogenic_subscore != 'UNKNOWN':
		if (pat_intensities['Very Strong'] >= 2):
			pathogenic_subscore = 'Pathogenic'
		elif (pat_intensities['Very Strong'] >= 1):
			if pat_intensities['Strong'] >= 1:
				pathogenic_subscore = 'Pathogenic'
			elif pat_intensities['Moderate'] >= 2:
				pathogenic_subscore = 'Pathogenic'
			elif (pat_intensities['Moderate'] ==1) & (pat_intensities['Supporting'] >=1):
				pathogenic_subscore = 'Pathogenic'
			elif pat_intensities['Supporting'] >=2:
				pathogenic_subscore = 'Pathogenic'
			elif pat_intensities['Moderate'] ==1:
				pathogenic_subscore = 'Likely Pathogenic'

		if ((pat_intensities['Strong'] >= 2) & (pat_intensities['Very Strong'] == 0)):
			pathogenic_subscore = 'Pathogenic'
		elif ((pat_intensities['Strong'] == 1) & (pat_intensities['Very Strong'] == 0)):
			if (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] >= 2): #modificato da >=1 supporting a >=2
				pathogenic_subscore = 'Likely Pathogenic'
			elif (((pat_intensities['Moderate'] == 1) | (pat_intensities['Moderate'] == 2)) & (pat_intensities['Supporting'] == 0)):
				pathogenic_subscore = 'Likely Pathogenic'
			elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Supporting'] >= 4):#modificato da >=3 supporting a >=4
				pathogenic_subscore = 'Pathogenic'
			elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Supporting'] < 4):#mod da <3 a <4
				pathogenic_subscore = 'Likely Pathogenic'
			elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Supporting'] >= 2):
				pathogenic_subscore = 'Pathogenic'
			elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Supporting'] < 2):
				pathogenic_subscore = 'Likely Pathogenic'
			elif pat_intensities['Moderate'] >= 3:
				pathogenic_subscore = 'Pathogenic'

		if ((pat_intensities['Moderate'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
			if pat_intensities['Supporting'] >= 3:
				pathogenic_subscore = 'Likely Pathogenic'
		elif (pat_intensities['Moderate'] >= 3):
			pathogenic_subscore =  'Likely Pathogenic'
		elif ((pat_intensities['Moderate'] >= 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
			if pat_intensities['Supporting'] == 1:
				pathogenic_subscore =  'Uncertain Significance'
			elif (pat_intensities['Supporting'] == 2):
				pathogenic_subscore =  'Likely Pathogenic'
			elif pat_intensities['Supporting'] >= 3:
				pathogenic_subscore =  'Likely Pathogenic'
	if benign_subscore != 'UNKNOWN':
		if ben_intensities['Stand Alone'] >= 1:
			benign_subscore = 'Benign'
		elif ben_intensities['Strong'] >= 2:
			benign_subscore = 'Benign'
		elif ben_intensities['Strong'] == 1:
			benign_subscore = 'Likely Benign'
		elif ben_intensities['Supporting'] >= 2:
			benign_subscore = 'Likely Benign'
	#############################################################################
	if (benign_subscore == 'Uncertain Significance') & (pathogenic_subscore != 'Uncertain Significance'):
		verdict = pathogenic_subscore
	elif (benign_subscore != 'Uncertain Significance'):# & (pathogenic_subscore == 'Uncertain Significance'): #questo serve per gestire quando ho 1 strong benigno e dei punti di patogenicità attivi
		if (ben_intensities['Strong'] == 1) & (ben_intensities['Stand Alone'] == 0) & (ben_intensities['Supporting'] == 0):
			if ((pat_intensities['Very Strong'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Moderate'] ==0) & (pat_intensities['Supporting'] == 0)):
				verdict = benign_subscore
			else:
				verdict = pathogenic_subscore
		else:
			if (pathogenic_subscore == 'Uncertain Significance'):
				verdict = benign_subscore
	return verdict

if __name__=="__main__":
	args=InputPar()
	folder_name = step.principal_folder(args,step.create_folder(),over=args.over)
	folder_final = join(folder_name,'final/')
	SAMPLE_to_analyze=sorted(glob.glob(join(folder_final,'*_variant_selection.csv')))

	for folder_sample in set(SAMPLE_to_analyze):
			sample_x = folder_sample.split('/')[-1]
			sample_x = str(sample_x)
			print ('----->'+sample_x+'<-----')
			#if True:
			#if (sample_x.split('_')[0]=='E38.2021'):
			SAMPLE = pd.read_csv(join(folder_final,sample_x), sep ='\t')
			SAMPLE_verdict = SAMPLE[['sample_id','HGVS','acmg_verdict','acmg_classificatins']]
			#print(SAMPLE_verdict[['sample_id','HGVS','acmg_classificatins']])
			SAMPLE_model = pd.DataFrame(columns=['sample_id','HGVS', 'BA1_final', 'BA1_intensita_final', 'BA1_cause_final', 'BS1_final', 'BS1_intensita_final', 'BS1_cause_final',
			'BS2_final', 'BS2_intensita_final', 'BS2_cause_final', 'BS3_final', 'BS3_intensita_final', 'BS3_cause_final', 'BS4_final', 'BS4_intensita_final', 'BS4_cause_final', 'BP1_final', 'BP1_intensita_final', 'BP1_cause_final',
			 'BP2_final', 'BP2_intensita_final', 'BP2_cause_final',  'BP3_final', 'BP3_intensita_final', 'BP3_cause_final', 'BP4_final', 'BP4_intensita_final', 'BP4_cause_final', 'BP5_final', 'BP5_intensita_final', 'BP5_cause_final',
			 'BP6_final', 'BP6_intensita_final', 'BP6_cause_final', 'BP7_final', 'BP7_intensita_final', 'BP7_cause_final', 'PVS1_final', 'PVS1_intensita_final', 'PVS1_cause_final', 'PS1_final', 'PS1_intensita_final', 'PS1_cause_final',
			 'PS2_final', 'PS2_intensita_final', 'PS2_cause_final', 'PS3_final', 'PS3_intensita_final', 'PS3_cause_final', 'PS4_final', 'PS4_intensita_final', 'PS4_cause_final', 'PM1_final', 'PM1_intensita_final', 'PM1_cause_final',
			 'PM2_final', 'PM2_intensita_final', 'PM2_cause_final', 'PM3_final', 'PM3_intensita_final', 'PM3_cause_final', 'PM4_final', 'PM4_intensita_final', 'PM4_cause_final', 'PM5_final', 'PM5_intensita_final', 'PM5_cause_final',
			 'PM6_final', 'PM6_intensita_final', 'PM6_cause_final', 'PP1_final', 'PP1_intensita_final', 'PP1_cause_final','PP2_final', 'PP2_intensita_final', 'PP2_cause_final', 'PP3_final', 'PP3_intensita_final', 'PP3_cause_final',
			 'PP4_final', 'PP4_intensita_final', 'PP4_cause_final', 'PP5_final', 'PP5_intensita_final', 'PP5_cause_final', 'choice_interpretation', 'choice_interpretation_final'])
			SAMPLE_model[['sample_id','HGVS', 'choice_interpretation']] = SAMPLE[['sample_id','HGVS','acmg_verdict']]
			SAMPLE_model = fill_checklist_model(SAMPLE_model, SAMPLE_verdict)
			#print(SAMPLE_model.loc[0,:])
			SAMPLE_model.to_csv(''.join([folder_final,sample_x.split('_')[0],'_interpretation_varsome.csv']), sep = '\t')
			#system(' '.join(['scp',(join(folder_final,sample_x.split('_')[0]+'_interpretation_varsome.csv')), "bioinfo@192.168.1.133:/home/bioinfo/VIRTUAL38/apimagi_dev/NGS_RESULT/CHECKLIST/"]))
			#system(' '.join(['scp',(join(folder_final,sample_x.split('_')[0]+'_interpretation_varsome.csv')), "bioinfo@192.168.1.55:/home/bioinfo/VIRTUAL38/apimagi_prod/NGS_RESULT/CHECKLIST/"]))
